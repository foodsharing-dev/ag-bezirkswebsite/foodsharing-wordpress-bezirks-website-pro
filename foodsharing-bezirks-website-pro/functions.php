<?php
/**
 * foodsharing bezirks child theme functions and definitions
 */

/*-----------------------------------------------------------------------------------*/
/* Include the parent theme style.css
/*-----------------------------------------------------------------------------------*/

function foodsharing_bezirks_child_styles() {
 
    $parent_style = 'foodsharing-bezirks-style';
 
    wp_deregister_style( $parent_style );
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'foodsharing-bezirks-child',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'foodsharing_bezirks_child_styles', 11 );




